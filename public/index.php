<?php

use Slim\Factory\AppFactory;
use DI\Container;

require __DIR__ . "/../vendor/autoload.php";

$container = new Container();

$container->set("templating", function () {
    return new Mustache_Engine([
        "loader" => new Mustache_Loader_FilesystemLoader(
            __DIR__ . "/../templates",
            ["extension" => ""]
        )
    ]);
});

$container->set("session", function () {
    return new \SlimSession\Helper();
});

$container->set("userService", function () {
    return new App\Service\UserService;
});

$container->set("contactService", function () {
    return new App\Service\ContactService;
});

AppFactory::setContainer($container);

$app = AppFactory::create();

$app->add(new \Slim\Middleware\Session([
    "autorefresh" => true,
    "lifetime" => "1 hour",
]));

$errorMiddleware = $app->addErrorMiddleware(true, true, true);
$errorMiddleware->setErrorHandler(
    Slim\Exception\HttpNotFoundException::class,
    function (Psr\Http\Message\ServerRequestInterface $request) use ($container) {
        $controller = new App\Controller\ExceptionController($container);
        return $controller->notFound($request);
    }
);

$app->any("/", "\App\Controller\AuthController:login");
$app->get("/logout", "\App\Controller\AuthController:logout");

$app->group("/contacts", function ($app) {
    $app->get("", "\App\Controller\ContactController:default");
    $app->get("/search", "\App\Controller\SearchController:search");
    $app->post("", "\App\Controller\ContactController:delete");
    $app->any("/add", "\App\Controller\ContactController:add");
    $app->any("/edit/{id:[0-9]+}", "\App\Controller\ContactController:edit");
})->add(new \App\Middleware\Authenticate($app->getContainer()->get("session")));


$app->run();
