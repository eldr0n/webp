<?php

namespace App\Controller;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response;

class ContactController extends Controller
{
    public function default(Request $request, Response $response): Response
    {
        $user = $this->ci->get("session")->get("user");
        $contacts = $this->ci->get("contactService")->getAll($user);
        return $this->render($response, "contacts.html", ["contacts" => $contacts]);
    }

    public function delete(Request $request, Response $response): Response
    {
        $selected = $request->getParsedBody();
        if ($selected) {
            $user = $this->ci->get("session")->get("user");
            foreach ($selected as $i) {
                $this->ci->get("contactService")->delete($user, $i);
            }
        }
        return $response->withRedirect("/contacts");
    }

    public function add(Request $request, Response $response): Response
    {
        if ($request->isPost()) {
            $user = $this->ci->get("session")->get("user");
            $data = $request->getParsedBody();
            $this->ci->get("contactService")->add($user, $data);

            return $response->withRedirect("/contacts");
        }

        return $this->render($response, "form.html", ["contact" => false]);
    }

    public function edit(Request $request, Response $response, $args = []): Response
    {
        $id = $args["id"];
        $user = $this->ci->get("session")->get("user");

        if ($request->isPost()) {
            $data = $request->getParsedBody();
            $this->ci->get("contactService")->edit($user, $id, $data);

            return $response->withRedirect("/contacts");
        }

        $contact = $this->ci->get("contactService")->getSingle($user, $id);

        return $this->render($response, "form.html", ["contact" => $contact]);
    }
}
