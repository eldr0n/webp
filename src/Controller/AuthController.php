<?php


namespace App\Controller;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response;

class AuthController extends Controller
{
    public function login(Request $request, Response $response): Response
    {
        if ($request->isPost()) {
            $username = $request->getParam("username");
            $pw = $request->getParam("password");
            $confpw = $request->getParam("confirm-password");

            if ($confpw === $pw) {
                $hash = password_hash($pw, PASSWORD_BCRYPT, ["cost" => 12]);
                $result = $this->ci->get("userService")->add($username, $hash);
            }

            $result = $this->ci->get("userService")->get($username);

            if (password_verify($pw, $result["password"])) {
                $this->ci->get("session")->set("user", $result["id"]);
                return $response->withRedirect("/contacts");
            }
        }

        return $this->render($response, "login.html");
    }

    public function logout(Request $request, Response $response): Response
    {
        $this->ci->get("session")->delete("user");
        return $response->withRedirect("/");
    }
}
