<?php

namespace App\Controller;

use Slim\Http\ServerRequest as Request;
use Slim\Http\Response;


class SearchController extends Controller
{

    public function search(Request $request, Response $response): Response
    {
        $user = $this->ci->get("session")->get("user");
        $contacts = $this->ci->get("contactService")->getAll($user);

        $query = strtolower($request->getQueryParam("q"));

        if ($query) {
            $contacts = array_values(array_filter($contacts, function ($contact) use ($query) {
                return str_contains(strtolower($contact["firstname"]), $query)
                    || str_contains(strtolower($contact["lastname"]), $query);
            }));
        }

        return $this->render($response, "contacts.html", [
            "contacts" => $contacts,
            "query" => $query
        ]);
    }
}
