<?php

namespace App\Service;

use PDO;
use PDOException;

abstract class Service
{
    protected $conn;

    public function __construct()
    {
        $host = "10.16.0.4";
        $user = "mariadb";
        $pass = "mariadb";
        $db = "mariadb";
        try {
            $this->conn = new PDO("mysql:host=$host;dbname=$db", $user, $pass);
        } catch (PDOException $e) {
            echo "Connection failed: " . $e->getMessage();
        }
    }
}
