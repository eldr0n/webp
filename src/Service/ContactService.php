<?php

namespace App\Service;

use PDO;

class ContactService extends Service
{
    public function getAll(int $userid)
    {
        $stmt = $this->conn->prepare("SELECT * FROM contacts WHERE userid = ?");
        $stmt->bindParam(1, $userid, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetchAll(PDO::FETCH_ASSOC);
    }

    public function delete(int $userid, int $id)
    {
        $stmt = $this->conn->prepare("DELETE FROM contacts WHERE userid = ? AND id = ?");
        $stmt->bindParam(1, $userid, PDO::PARAM_INT);
        $stmt->bindParam(2, $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function add(int $userid, array $data)
    {
        $stmt = $this->conn->prepare(
            "INSERT INTO contacts (userid, firstname, lastname, street, city, zip, country, phone, mail) 
            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)"
        );
        $stmt->bindParam(1, $userid, PDO::PARAM_INT);
        $stmt->bindParam(2, $data["firstname"], PDO::PARAM_STR);
        $stmt->bindParam(3, $data["lastname"], PDO::PARAM_STR);
        $stmt->bindParam(4, $data["street"], PDO::PARAM_STR);
        $stmt->bindParam(5, $data["city"], PDO::PARAM_STR);
        $stmt->bindParam(6, $data["zip"], PDO::PARAM_STR);
        $stmt->bindParam(7, $data["country"], PDO::PARAM_STR);
        $stmt->bindParam(8, $data["phone"], PDO::PARAM_STR);
        $stmt->bindParam(9, $data["mail"], PDO::PARAM_STR);
        $stmt->execute();
    }

    public function edit(int $userid, int $id, array $data)
    {
        $stmt = $this->conn->prepare(
            "UPDATE contacts SET firstname = ?, lastname = ?,
            street = ?, city = ?, zip = ?, country = ?, 
            phone = ?, mail = ? WHERE userid = ? AND id = ?"
        );
        $stmt->bindParam(1, $data["firstname"], PDO::PARAM_STR);
        $stmt->bindParam(2, $data["lastname"], PDO::PARAM_STR);
        $stmt->bindParam(3, $data["street"], PDO::PARAM_STR);
        $stmt->bindParam(4, $data["city"], PDO::PARAM_STR);
        $stmt->bindParam(5, $data["zip"], PDO::PARAM_STR);
        $stmt->bindParam(6, $data["country"], PDO::PARAM_STR);
        $stmt->bindParam(7, $data["phone"], PDO::PARAM_STR);
        $stmt->bindParam(8, $data["mail"], PDO::PARAM_STR);
        $stmt->bindParam(9, $userid, PDO::PARAM_INT);
        $stmt->bindParam(10, $id, PDO::PARAM_INT);
        $stmt->execute();
    }

    public function getSingle(int $userid, int $id)
    {
        $stmt = $this->conn->prepare("SELECT * FROM contacts WHERE userid = ? AND id = ?");
        $stmt->bindParam(1, $userid, PDO::PARAM_INT);
        $stmt->bindParam(2, $id, PDO::PARAM_INT);
        $stmt->execute();
        return $stmt->fetch(PDO::FETCH_ASSOC);
    }
}
