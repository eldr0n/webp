-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 10.16.0.4
-- Generation Time: Mar 10, 2021 at 09:32 PM
-- Server version: 10.4.18-MariaDB-1:10.4.18+maria~focal
-- PHP Version: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mariadb`
--

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `userid` int(11) NOT NULL,
  `firstname` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `lastname` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL,
  `street` varchar(64) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `city` varchar(64) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `zip` varchar(64) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `country` varchar(64) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `phone` varchar(64) COLLATE utf8_general_mysql500_ci DEFAULT NULL,
  `mail` varchar(64) COLLATE utf8_general_mysql500_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `userid`, `firstname`, `lastname`, `street`, `city`, `zip`, `country`, `phone`, `mail`) VALUES
(1, 1, 'Hampi', 'Hobler', 'Spirtzlstrasse 11', 'Chur', '7000', 'Schweiz', '0811234567', 'hampi@hobler.ch'),
(2, 1, 'Ynes', 'Ockleshaw', '37 Hoard Avenue', 'Fernández', '4322', 'Argentina', '2028124828', 'yockleshaw0@trellian.com'),
(3, 1, 'Sanderson', 'Baxendale', '5237 Novick Lane', 'Cileunyi', NULL, 'Indonesia', '9388250317', 'sbaxendale1@list-manage.com'),
(4, 1, 'Cassaundra', 'Quickenden', '8 Stephen Center', 'Kendung Timur', NULL, 'Indonesia', '8101370180', 'cquickenden2@china.com.cn'),
(5, 1, 'Brittan', 'Hartil', '23395 Kennedy Junction', 'Vila Chã', '3720-727', 'Portugal', '3374357486', 'bhartil3@hp.com'),
(6, 1, 'Sibeal', 'Bryers', '557 Summerview Center', 'Benevides', '68795-000', 'Brazil', '1407306584', 'sbryers4@facebook.com'),
(7, 1, 'Darleen', 'Loseby', '379 Burrows Drive', 'Kipini', NULL, 'Kenya', '3483480083', 'dloseby5@foxnews.com'),
(8, 1, 'Dene', 'Bellhanger', '288 Ridgeview Lane', 'Guintubhan', '7008', 'Philippines', '3905015288', 'dbellhanger6@cisco.com'),
(9, 1, 'Jaclyn', 'Cowthart', '6212 Hermina Way', 'Belyy Yar', '662927', 'Russia', '8409829618', 'jcowthart7@zdnet.com'),
(10, 1, 'Lynnea', 'Cudbertson', '08 Straubel Way', 'Novhorod-Sivers’kyy', NULL, 'Ukraine', '4762131425', 'lcudbertson8@cisco.com'),
(11, 1, 'Sheilah', 'Downton', '5 Merchant Plaza', 'Qaşr al Farāfirah', NULL, 'Egypt', '3724948398', 'sdownton9@yahoo.com'),
(12, 1, 'Raeann', 'Benwell', '95345 Hintze Hill', 'Apóstoles', '3358', 'Argentina', '1285875608', 'rbenwella@apple.com'),
(13, 1, 'Susy', 'Droghan', '886 Amoth Court', 'El Agua Dulcita', NULL, 'Honduras', '3336237955', 'sdroghanb@aol.com');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(12) COLLATE utf8_general_mysql500_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_general_mysql500_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_mysql500_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'eldr0n', '$2y$12$L2PitaoON9xoo7Vhdj3EiOYNzZuqQAQ4j3bdPEqn8e0oJxxaDEvcq');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `userid` (`userid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `contacts`
--
ALTER TABLE `contacts`
  ADD CONSTRAINT `contacts_ibfk_1` FOREIGN KEY (`userid`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
